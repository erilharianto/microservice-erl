package id.co.sigma.minicoredb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.minicoredb.entity.Role;

public interface RoleDao extends JpaRepository<Role, Long> {

    Role findByAuthority(String roleName);

}
