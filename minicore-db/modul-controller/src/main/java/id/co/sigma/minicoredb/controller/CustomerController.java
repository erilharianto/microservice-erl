package id.co.sigma.minicoredb.controller;

import java.util.ArrayList;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import id.co.sigma.minicoredb.model.Customer;
import id.co.sigma.minicoredb.response.MessageResponse;
import id.co.sigma.minicoredb.service.CustomerService;

@RestController
public class CustomerController{
	
	@Autowired
	private CustomerService service;
	


	@RequestMapping(path = "customer/get/all")
	public List<Customer> getAll() {
		return service.findAll();
	}

	@RequestMapping(path = "customer/get/{id}")
	public Customer getById(@PathVariable("id") Long id) {
		return service.findById(id);
	}
	
	@RequestMapping(path = "customer/delete/{id}")
	public List<Customer> deleteById(@PathVariable("id") Long id) {
		service.delete(id);
		return getAll();
	}
	
	@RequestMapping(path = "customer/add")
	public MessageResponse add(@RequestBody Customer customer) {
		String errorMsg;
		try {
			service.save(customer);
			errorMsg = "SUCCESS";
			MessageResponse message = new MessageResponse(errorMsg);
			return message;
		} catch (DataIntegrityViolationException ex) {
			errorMsg = "FAILED";
			MessageResponse message = new MessageResponse(errorMsg);
			return message;
		} catch (NullPointerException e) {
			errorMsg = "ERROR";
			MessageResponse message = new MessageResponse(errorMsg);
			return message;
		}
	}
	
	@RequestMapping(path = "customer/update")
	public MessageResponse update(@RequestBody Customer customer) {
		String errorMsg;
		Customer cust = service.findById(customer.getIdCustomer());
		try {
			if (customer.getName()==null || cust.getName() == customer.getName())
				customer.setName(cust.getName());
			if (customer.getAddress()==null || cust.getAddress() == customer.getAddress())
				customer.setAddress(cust.getAddress());
			if(customer.getPhoneNumber() ==null || cust.getPhoneNumber() == cust.getPhoneNumber())
				customer.setPhoneNumber(cust.getPhoneNumber());
			service.save(customer);
			errorMsg = "SUCCESS";
			
			return new MessageResponse(errorMsg);
		} catch (DataIntegrityViolationException ex) {
			errorMsg = "FAILED";
			return new MessageResponse(errorMsg);
		} catch (NullPointerException e) {
			errorMsg = "ERROR";
			return new MessageResponse(errorMsg);
		}
	}

}
