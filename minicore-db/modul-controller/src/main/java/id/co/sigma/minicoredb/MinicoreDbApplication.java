package id.co.sigma.minicoredb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinicoreDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinicoreDbApplication.class, args);
	}

}
