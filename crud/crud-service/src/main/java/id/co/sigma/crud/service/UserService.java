package id.co.sigma.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.crud.feign.MinicoreInterface;

@Service("userService")
public class UserService {
		
	@Autowired
	private MinicoreInterface interfaceMinicore;
	
	public List listUser() {
		return interfaceMinicore.listUser();
	}

}
