package id.co.sigma.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.crud.model.Account;
import id.co.sigma.crud.response.MessageResponse;
import id.co.sigma.crud.service.AccountService;
import id.co.sigma.crud.service.CustomerService;

@RestController
public class AccountController {
	
	@Autowired
	private AccountService service;
	
	@RequestMapping(path = "account/get/all")
	public List<Account> getAll(){
		return service.findAll();
	}
	
	@RequestMapping(path = "acccount/get/{id}")
	public Account getById(@PathVariable("id") Long id) {
		return service.findById(id);
	}
	
	@RequestMapping(path ="account/add")
	public MessageResponse add(@RequestBody Account account) {
		return service.add(account);
	}
	
	@RequestMapping(path = "account/delete/{id}")
	public List<Account> deleteById(@PathVariable("id") Long id){
		return service.deleteById(id);
	}
	
	@RequestMapping(path = "account/inquiry/all/{id}")
	public List<Account> inquiryAll(@PathVariable("id") Long id){
		return service.inquiryAll(id);
	}
	
//
//	@Autowired
//	private CustomerService custService;
//
//	@RequestMapping(path = "account/get/all")
//	public List<Account> getAll() {
//		return service.findAll();
//	}
//
//	@RequestMapping(path = "account/get/{id}")
//	public Account getById(@PathVariable("id") Long id) {
//		return service.findById(id);
//	}
//
//	@RequestMapping(path = "account/delete/{id}")
//	public List<Account> deleteById(@PathVariable("id") Long id) {
//		service.delete(id);
//		return getAll();
//	}

}
