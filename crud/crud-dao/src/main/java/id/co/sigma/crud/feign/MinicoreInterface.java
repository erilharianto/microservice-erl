package id.co.sigma.crud.feign;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import feign.auth.BasicAuthRequestInterceptor;
import id.co.sigma.crud.model.Account;
import id.co.sigma.crud.model.Customer;
import id.co.sigma.crud.response.AuthTokenInfo;
import id.co.sigma.crud.response.MessageResponse;

@FeignClient("minicore")
public interface MinicoreInterface {

	// Customer Services

	@RequestMapping(path = "/minicore/customer/get/all")
	public List<Customer> getAll();

	@RequestMapping(path = "minicore/customer/add")
	public MessageResponse addCustomer(@RequestBody Customer customer);

	@RequestMapping(path = "minicore/customer/update")
	public MessageResponse updateCustomer(@RequestBody Customer customer);

	@RequestMapping(path = "minicore/customer/delete/{id}")
	public List<Customer> deleteByIdCustomer(@PathVariable("id") Long id);

	@RequestMapping(path = "minicore/customer/get/{id}")
	public Customer getByIdCustomer(@PathVariable("id") Long id);

	// Account Services

	@RequestMapping(path = "minicore/account/get/all")
	public List<Account> getAllAccount();

	@RequestMapping(path = "minicore/account/get/{id}")
	public Account getByIdAccount(@PathVariable("id") Long id);

	@RequestMapping(path = "minicore/account/add")
	public MessageResponse addAccount(@RequestBody Account account);

	@RequestMapping(path = "minicore/account/delete/{id}")
	public List<Account> deleteByIdAccount(@PathVariable("id") Long id);

	@RequestMapping(path = "account/inquiry/{id}/all")
	public List<Account> inquiryAll(@PathVariable("id") Long idCustomer);

	@RequestMapping(value = "/minicore/webservice/listUser", method = { RequestMethod.GET })
	List listUser();
}
